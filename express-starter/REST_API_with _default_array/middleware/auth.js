const { checkUser } = require('./../model/database');
const  error = require('./error');

var middleware = function(req,res,next) {
    let id = req.param('id');
    let result = checkUser(id);
    if(result == true)
    next();
    else
    res.json(error.errormsg);
}

module.exports.middleware1 = middleware;