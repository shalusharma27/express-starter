const express = require('express');
const app = express();
const { port } = require('./config/config');
const router = require('./routes/routes');
const bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json())

app.use(router);

app.listen(port,() => {
    console.log("Server is Started at!!!!!..."+port);
})