 let count = 3;
 const database = [
     {
        _id : 1,
        name : 'Nick',
        age : 23
     },
     {
         _id : 2,
         name : 'Billy',
         age : 43
    }]

    const getUser = () => {
        return database;
    }
    const getUserById = (id) => {
       let temp = database.filter((item) => {
            return item._id == id
        });
        return temp;
    }
    const addUser = (user) => {
        user._id = count++;
        database.push(user);
    }
    const updateUser = (id,user) => {
        let temp = database.filter((item) => {
            if(item._id == id)
            item.name = user.name;
            item.age = user.age;
        });
        return "User Updated"
    }
    const deleteUser = (id) => {
        let temp = database.filter((item) => {
            if(item._id == id )
            {
            let index = database.indexOf(item);
            if(index> -1)
            database.splice(index,1);
            }
        });
        return "User Deleted"
    }
    const checkUser = (id) => {
        if(database.filter((item) => item._id ==id ).length != 0)
        return true;
    }
    module.exports = {
        getUser,
        getUserById,
        addUser,
        updateUser,
        deleteUser,
        checkUser
    }