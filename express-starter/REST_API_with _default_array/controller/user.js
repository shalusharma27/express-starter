const { getUser, getUserById, addUser, updateUser, deleteUser } = require('./../model/database');

let printUser = ( req,res ) => {
    let users = getUser();
    res.json(users);
};

let printUserById = ( req,res ) => {
    let id = req.param('id');
    let users = getUserById(id);
    res.json(users);
};

let plusUser = ( req,res ) => {
    let user = req.body;
    let users = addUser(user);
    res.json("User Added");
};

let modifyUser = ( req,res ) => {
    let id = req.param('id');
    let user = req.body;
    let users = updateUser(id,user);
    res.json(users)
}

let removeUser = ( req, res ) => {
    let id = req.param('id');
    let users = deleteUser(id);
    res.json(users);
}

module.exports.getUser2 = printUser;
module.exports.getUserById2 = printUserById;
module.exports.addUser2 = plusUser;
module.exports.updateUser2 = modifyUser;
module.exports.deleteUser2  = removeUser;