const express = require('express');
const data = require('./../controller/user');
const check = require('./../middleware/auth');
const router = express.Router();


router.get('/users',data.getUser2);
router.get('/users/:id',check.middleware1,data.getUserById2);
router.post('/users',data.addUser2);
router.put('/users/:id',check.middleware1,data.updateUser2);
router.delete('/users/:id',check.middleware1,data.deleteUser2);
module.exports = router;