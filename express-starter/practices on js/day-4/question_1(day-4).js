const arr=[];
let obj1=
{   authorname:"JK rowling",
    bookname:"Harry Potter",
    chk:true
};
arr.push(obj1);
let obj2=
{
    authorname:"Chetan Bhagat",
    bookname:"Life As We Know it",
    chk:false
};
arr.push(obj2);
let obj3=
{
    authorname:"Smith",
    bookname:"The Notebook",
    chk:false
};
arr.push(obj3);
let obj4=
{
    authorname:"Smith",
    bookname:"Tom Jonas",
    chk:true
};
arr.push(obj4);
let obj5=
{
    authorname:"JK rowling",
    bookname:"Mistakes",
    chk:false
};
arr.push(obj5);
authoradd();
for(let i=0;i<arr.length;++i)
{
    addRowInTable(arr[i]);
}
function addRowInTable(arg)
{

    console.log(arg);   
    var table = document.getElementById("tableid");
    var row = table.insertRow(-1);
    var cell1 = row.insertCell(-1);
    var cell2 = row.insertCell(-1);
    var cell3 = row.insertCell(-1);
    var cell4 = row.insertCell(-1);

    cell1.innerHTML = arg.authorname;
    cell2.innerHTML = arg.bookname;
    cell3.innerHTML = arg.chk;
    var element=document.createElement("button");
    element.type="submit";
    element.name="delete";
    element.textContent="Delete";
    cell4.appendChild(element);
    element.setAttribute("onclick","deleterow(this)")

}
function addrow()
{
    let authorname=document.getElementById("authorname").value;
    let bookname=document.getElementById("bookname").value;
    let chk=document.getElementById("chk");

    let nameRegex = /^[a-zA-Z ]/;

    if(!nameRegex.test(authorname)){
        alert("Please enter a valid author name");
    }
    else if(!nameRegex.test(bookname)){
        alert("Please enter a valid book name");
    }
    else{

        //everything is valid, now we can submit the entry
        //creating object
        const obj = {};
        Object.defineProperties(obj,{
            authorname:{
                value:authorname,
                writable: true
            },
            bookname:{
                value:bookname,
                writable:true
            },
            chk:{
                value:chk.checked,
                writable:true
            }
        });
        arr.push(obj);
        addRowInTable(obj);
        alert("Submitted Successfully!!");
        authoradd();
    }
    reset();
}
function deleterow(r) 
{
    var i = r.parentNode.parentNode.rowIndex;
    document.getElementById("tableid").deleteRow(i);
}

function searchbook() 
{
    let input;
    input = document.getElementById("search").value;
    let sea=[];
    let regex = new RegExp(input,'i');
    for(let i=0;i<arr.length;i++)
    {
        if(regex.test(arr[i].bookname))
        sea.push(arr[i]);
        
    }   display(sea);
    
}


function drop(value)
{
    let result=arr.filter(item => {
        return value==item.authorname;

    });
    
    display(result);

    
}


function display(disp)
{
    let row=document.getElementById("tableid").getElementsByTagName('tr');
    let len=row.length;
    for(let i=1;i<len;i++)
    {
        document.getElementById("tableid").deleteRow(i);
        i--;
        len--;

    }
    for(let i=0;i<arr.length;i++)
    {
       addRowInTable(disp[i]);
    }

}

function reset() 
{
    document.getElementById("resetform").reset();
}

function authoradd()
{   
    let drop=new Set();
    document.getElementById("dropdown").innerHTML="<option selected disabled>Select Author</option>";
    let add_a=document.getElementById("dropdown");
    for(let i=0;i<arr.length;i++)
    {
        drop.add(arr[i].authorname);
    }
    for(let item of drop)
    {
        let option=document.createElement("option");
        option.text=item;
        add_a.add(option);
    }
}
let time=0;
function sortauthor()
{   let a=0;
    if(time==0)
    {
        sortascending(a);
        time++;
    }
    else
    if(time==1)
    {
        sortdescending(a);
        time=0;

    }
}
function sortbook()
{   let b=1;
    if(time==0)
    {
        sortascending(b);
        time++;
    }
    else
    if(time==1)
    {
        sortdescending(b);
        time=0;

    }
}

function sortascending(a) 
{
    var table, rows, switching, i, x, y, shouldSwitch;
    table = document.getElementById("tableid");
    switching = true;
    while (switching) 
    {   switching = false;
        rows = table.getElementsByTagName("tr");
        for (i = 1; i < (rows.length - 1); i++) 
        {
            shouldSwitch = false;
            x = rows[i].getElementsByTagName("td")[a];
            y = rows[i + 1].getElementsByTagName("td")[a];
            if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) 
            {
                shouldSwitch = true;
                break;
            }
        }
      if (shouldSwitch) 
      {     rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
      }
    }
}

function sortdescending() 
{
    var table, rows, switching, i, x, y, shouldSwitch;
    table = document.getElementById("tableid");
    switching = true;
    while (switching) 
    {   switching = false;
        rows = table.getElementsByTagName("tr");
        for (i = 1; i < (rows.length - 1); i++) 
        {
            shouldSwitch = false;
            x = rows[i].getElementsByTagName("td")[0];
            y = rows[i + 1].getElementsByTagName("td")[0];
            if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) 
            {
                shouldSwitch = true;
                break;
            }
        }
      if (shouldSwitch) 
      {     rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
      }
    }
}

let currentPage=1;
let pagePerRecord=2;
function prevpage()
{
    if(currentPage>1)
    currentPage--;
    console.log("hello");
    changepage(currentPage);
}
function nextpage()
{
    if(currentPage<numpage())
    currentPage++;
    changepage(currentPage);
}
function numpage()
{
    return Math.ceil(arr.length/pagePerRecord);
}


function changepage(page)
{
    var btn_next = document.getElementById("butnext");
    var btn_prev = document.getElementById("butprev");
    var listing_table = document.getElementById("tableid");
    if (page < 1) page = 1;
    if (page > numpage()) page = numpage();
    listing_table.innerHTML="";

    for (var i = (page-1) * pagePerRecord; i < (page * pagePerRecord); i++) {
        listing_table.innerHTML += arr[i].authorname+arr[i].bookname+arr[i].chk + "<br>";
        
    }


    if (page == 1) {
        btn_prev.style.visibility = "hidden";
    } else {
        btn_prev.style.visibility = "visible";
    }

    if (page == numpage()) {
        btn_next.style.visibility = "hidden";
    } else {
        btn_next.style.visibility = "visible";
    }
}
