var fs = require('fs');
var http = require('http');

const file = fs.createWriteStream('big.txt');

for(let i=0;i<=1e6;i++)
{
    file.write("Lorem ipsum dolor sit meet, consectetur adipisicing elit,sed do eiusmod tempor incididunt ut  labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proiden.\n\n'");
}
file.end();

var server = http.createServer(function(){
server.on('request',(req,res) =>
{
        let data = '';
        let readStream = fs.createReadStream('big.txt'); 
        readStream.setEncoding('UTF8');
        readStream.on('data', function(chunk) 
        { 
            data += chunk;
            readStream.pipe(res);
        });
        readStream.on('end', function()
        { 
            res.end();
        });
    })
});
server.listen(8080);



