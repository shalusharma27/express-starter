var http=require('http');
var url=require('url');
var stringquery=require('querystring');

var server=http.createServer(function(req,res){
    var par=stringquery.parse(url.parse(req.url).query);
    console.log(par);
    res.writeHead(200,{'Content-Type':'text/html'});
    if('firstname' in par && 'lastname' in par)
    res.write('your name is'+' '+par['firstname']+' '+'last name is'+' '+par['lastname']);
    else
    res.write("what's your name");
    res.end();
});

server.listen(8081);
