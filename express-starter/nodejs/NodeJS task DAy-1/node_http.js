var http = require("http");
var fs = require('fs');
var url = require('url');
var stringquery=require('querystring');
var array=[];


var server = http.createServer(function (req, res) {

    if(req.url=="/home"){

        fs.readFile('./node_html.html', function(err, data) {
        res.writeHead(200, {'Content-Type': 'text/html'});
        if(err)
        {
            res.writeHead(404);
            res.write('not found');
            res.end();

        }
        else
        {
        res.write(data);
        res.end();
    
        } 
        });  
    }
else if(req.url === "/resume"){
    fs.readFile("./resume.html", "UTF-8", function(err, html){
        res.writeHead(200, {"Content-Type": "text/html"});
        res.write(html);
        res.end();
    });
}
else if(req.url.match('/tech*')){
    let url=req.url;
    str=url.split('?')[1];
    var a = stringquery.parse(str);
    switch(a.technology)
    {
        case 'js':
        {
            fs.readFile('./js.html',null,function(err,data){
            if(err)
            {
                res.writeHead(404);
                res.write('not found');
                res.end();

            }
            else
            {
            res.write(data);
            res.end();

            }  
          
            });
        }
        break;
        case 'nodejs':
        {
            fs.readFile('./nodejs.html',null,function(err,data){
            if(err)
            {
                res.writeHead(404);
                res.write('not found');
                res.end();

            }
            else
            {
            res.write(data);
            res.end();

  
            }  
            });
        }
        break;
        case 'html':
        {
            fs.readFile('./html.html',null,function(err,data){
            if(err)
            {
                res.writeHead(404);
                res.write('not found');
                res.end();

            }
            else
            {
            res.write(data);
            res.end();

            }  
            });
        }
        break;
        case 'python':
        {
            fs.readFile('python.html',null,function(err,data){
            if(err)
            {
                res.writeHead(404);
                res.write('not found');
                res.end();

            }
            else
            {
            res.write(data);
            res.end();

            }  
            });
        }
        break;
       default:
       {
           res.write("Can Not Load The Page:")
           res.end();
       }
    }
}else if(req.url.match('/form*')){
    let url=req.url;
    str=url.split('?')[1];
    let a = stringquery.parse(str);
    let name = a.name;
    let feedback =a.feedback;
    let data="Name:"+name+"  "+"feedback:"+feedback;
    array.push(data);
    console.log(array);
    res.end();
}
else
{
    res.write("Not Found");
    res.end();
}

});
server.listen(8080);
console.log('Server running at http://127.0.0.1:8080/');
