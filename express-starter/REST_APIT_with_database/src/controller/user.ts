import User from './../model/user';
class UserController {

    public createUser(req , res, next): void {
        const userObject = req.body;
        User.addUser(userObject)
            .then((doc: any) => {
                res.send({status: 'OK' , data: doc._id});
            })
            .catch((err: Error) => next(new Error(err.message)));
    }
    public updateUser(req, res, next): void {
        const id = req.params.id;
        const userObject = req.body;
        User.improveUser(id, userObject)
            .then((doc: any) => res.send({status: 'OK', data: doc._id}))
            .catch((err: Error) => next(new Error(err.message)));

    }
    public deleteUser(req, res, next): void {
        const id = req.params.id;
        User.removeUser(id)
            .then((doc: any) => res.send({status: 'OK', data: 'null'}))
            .catch((err: Error) => next(new Error(err.message)));

    }
    public getUser(req, res, next): void {
        User.displayAllUsers()
            .then((doc): any => res.send({status: 'OK', data: doc}));
    }

    public getUserById(req, res, next): void {
        const id = req.params.id;
        User.displayUser(id)
            .then((doc: string) => res.json(doc))
            .catch((err: Error) => next(new Error(err.message)));
    }
}

export default new UserController();
