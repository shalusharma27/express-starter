import * as mongoose from 'mongoose';
import { dbURL } from './config/config';

class Data  {
    public connect() {
        return mongoose.connect(dbURL, {useNewUrlParser: true});
    }
}

export default new Data();
