import * as mongoose from 'mongoose';
const Schema = mongoose.Schema;

const User = new Schema({
    email: { type: String, lowercase: true, trim: true, unique: true },
    id: Number,
    name: String,
    password: String,
    userName: { type: String, unique: true },
    });

User.statics = {
    displayAllUsers: function displayAllUsers() {
        return this.find();
    },
    improveUser(id: string, user: string) {
        return this.findOneAndUpdate({ _id: id }, { $set: user }, { upsert: true, new: true });
    },
    removeUser(id: string) {
        return this.findOneAndRemove({ _id: id });
    },
    displayUser(id: string) {
        return this.findOne({ _id: id });
    },
    addUser(user: string) {
        const data = new this(user);
        return data.save();
    },
};

interface IUserDocument extends mongoose.Document {
    id: number;
    name: string;
    userName: { type: string, unique: boolean };
    email: { type: string, lowercase: boolean, trim: boolean, unique: boolean };
}
interface IUserModel extends mongoose.Model<IUserDocument> {
    displayAllUsers: () => any;
    improveUser(id: string, user: string): any;
    removeUser(id: string): any;
    displayUser(id: string): any;
    addUser(id: string): any;
}

export default  mongoose.model<IUserDocument, IUserModel>('User', User);
