import * as express from 'express';
import { validationResult } from 'express-validator/check';
import checkUser from './../middleware/check-auth';
const router: express.Router = express.Router();
import data from './../controller/user';
import UserValidator from './../validation/user';

router.get('/users', checkUser, data.getUser);

router.get('/users/:id', data.getUserById);

router.post('/users', UserValidator, (req, res, next) => {
    const notValid = validationResult(req);
    if (notValid.isEmpty()) {
        next();
    }
    else {
        res.status(500).send(notValid.array());
    }
}, data.createUser);

router.put('/users/:id', UserValidator, (req, res, next) => {
    const notValid = validationResult(req);
    if (notValid.isEmpty()) {
        next();
    }
    else {
        res.status(500).send(notValid.array());
    }
}, data.updateUser);

router.delete('/users/:id', data.deleteUser);

export default router;
