import { checkSchema } from 'express-validator/check';

const UserValidator = checkSchema({
    email: {
        exists: {
            errorMessage: 'Email cannot be empty',
        },
        in: ['body'],
        isEmail: {
            errorMessage: 'Invalid email',
        },
    },
    name: {
        exists: {
            errorMessage: 'Length of name cannot be less than 1 and greater than 20 characters',
        },
        in: ['body'],
        isAlpha: {
            errorMessage: 'firstName must contain only alphabets',
        },
    },
    userName: {
        exists: {
            errorMessage: 'lastName cannot be empty',
            },
        in: ['body'],
        matches: {
            errorMessage: 'Invalid lastName',
            options: '^[a-zA-Z0-9_]+$',
            },
    },
});
export default UserValidator;
