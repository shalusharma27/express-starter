import * as bodyParser from 'body-parser';
import * as express from 'express';
import { port } from './config/config';
import Data from './dbconnection';
import router from './routes/routes';

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(router);

app.use((err: Error, req: express.Request, res: express.Response, next: express.NextFunction): void => {
    console.log('Middleware is working');
    res.send({status: 'error' , message: err.message});
});

Data.connect().then(() => {
app.listen(port, (): void => {
    console.log('Server is started at ....!!!', port);
    });
});
