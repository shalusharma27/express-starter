"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const bodyParser = require("body-parser");
const express = require("express");
// import * as jwt from 'jsonwebtoken';
const config_1 = require("./config/config");
const dbconnection_1 = require("./dbconnection");
const routes_1 = require("./routes/routes");
const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(routes_1.default);
app.use((err, req, res, next) => {
    console.log('Middleware is working');
    res.send({ status: 'error', message: err.message });
});
dbconnection_1.default.connect().then(() => {
    app.listen(config_1.port, () => {
        console.log('Server is started at ....!!!', config_1.port);
    });
});
//# sourceMappingURL=server.js.map