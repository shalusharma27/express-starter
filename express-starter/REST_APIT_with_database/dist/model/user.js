"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const User = new Schema({
    email: { type: String, lowercase: true, trim: true, unique: true },
    id: Number,
    name: String,
    password: String,
    userName: { type: String, unique: true },
});
User.statics = {
    displayAllUsers: function displayAllUsers() {
        return this.find();
    },
    improveUser(id, user) {
        return this.findOneAndUpdate({ _id: id }, { $set: user }, { upsert: true, new: true });
    },
    removeUser(id) {
        return this.findOneAndRemove({ _id: id });
    },
    displayUser(id) {
        return this.findOne({ _id: id });
    },
    addUser(user) {
        const data = new this(user);
        return data.save();
    },
};
exports.default = mongoose.model('User', User);
//# sourceMappingURL=user.js.map