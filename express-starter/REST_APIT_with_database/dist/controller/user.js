"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const user_1 = require("./../model/user");
class UserController {
    createUser(req, res, next) {
        const userObject = req.body;
        user_1.default.addUser(userObject)
            .then((doc) => {
            res.send({ status: 'OK', data: doc._id });
        })
            .catch((err) => next(new Error(err.message)));
    }
    updateUser(req, res, next) {
        const id = req.params.id;
        const userObject = req.body;
        user_1.default.improveUser(id, userObject)
            .then((doc) => res.send({ status: 'OK', data: doc._id }))
            .catch((err) => next(new Error(err.message)));
    }
    deleteUser(req, res, next) {
        const id = req.params.id;
        user_1.default.removeUser(id)
            .then((doc) => res.send({ status: 'OK', data: 'null' }))
            .catch((err) => next(new Error(err.message)));
    }
    getUser(req, res, next) {
        user_1.default.displayAllUsers()
            .then((doc) => res.send({ status: 'OK', data: doc }));
    }
    getUserById(req, res, next) {
        const id = req.params.id;
        user_1.default.displayUser(id)
            .then((doc) => res.json(doc))
            .catch((err) => next(new Error(err.message)));
    }
}
exports.default = new UserController();
//# sourceMappingURL=user.js.map