"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const check_1 = require("express-validator/check");
const UserValidator = check_1.checkSchema({
    email: {
        exists: {
            errorMessage: 'Email cannot be empty',
        },
        in: ['body'],
        isEmail: {
            errorMessage: 'Invalid email',
        },
    },
    name: {
        exists: {
            errorMessage: 'Length of name cannot be less than 1 and greater than 20 characters',
        },
        in: ['body'],
        isAlpha: {
            errorMessage: 'firstName must contain only alphabets',
        },
    },
    userName: {
        exists: {
            errorMessage: 'lastName cannot be empty',
        },
        in: ['body'],
        matches: {
            errorMessage: 'Invalid lastName',
            options: '^[a-zA-Z0-9_]+$',
        },
    },
});
exports.default = UserValidator;
//# sourceMappingURL=user.js.map