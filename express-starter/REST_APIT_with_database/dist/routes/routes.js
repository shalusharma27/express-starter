"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const check_1 = require("express-validator/check");
const router = express.Router();
const user_1 = require("./../controller/user");
const user_2 = require("./../validation/user");
router.get('/users', user_1.default.getUser);
router.get('/users/:id', user_1.default.getUserById);
router.post('/users', user_2.default, (req, res, next) => {
    const notValid = check_1.validationResult(req);
    if (notValid.isEmpty()) {
        next();
    }
    else {
        res.status(500).send(notValid.array());
    }
}, user_1.default.createUser);
router.put('/users/:id', user_2.default, (req, res, next) => {
    const notValid = check_1.validationResult(req);
    if (notValid.isEmpty()) {
        next();
    }
    else {
        res.status(500).send(notValid.array());
    }
}, user_1.default.updateUser);
router.delete('/users/:id', user_1.default.deleteUser);
exports.default = router;
//# sourceMappingURL=routes.js.map