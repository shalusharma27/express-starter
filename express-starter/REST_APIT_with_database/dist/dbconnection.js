"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const config_1 = require("./config/config");
class Data {
    connect() {
        return mongoose.connect(config_1.dbURL, { useNewUrlParser: true });
    }
}
exports.default = new Data();
//# sourceMappingURL=dbconnection.js.map