"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const swaggerJSDoc = require("swagger-jsdoc");
const swaggerDefinition = {
    basePath: '/',
    host: 'localhost:3000',
    info: {
        description: 'Demonstrating how to describe a RESTful API with Swagger',
        parameters: {},
        paths: {},
        responses: {},
        swagger: '2.0',
        title: 'Node Swagger API',
        version: '1.0.0',
    },
    security: [
        { Bearer: [] },
    ],
    securityDefinitions: {
        Bearer: {
            in: 'Headers',
            name: 'Authorization',
            type: 'apiKey',
        },
    },
};
const options = {
    apis: ['./dist/**/*.js', './dist/router.js'],
    swaggerDefinition,
};
const swaggerSpec = swaggerJSDoc(options);
exports.default = swaggerSpec;
//# sourceMappingURL=swagger.js.map