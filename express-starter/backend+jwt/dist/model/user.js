"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
/**
 * @swagger
 * definitions:
 *   users:
 *     properties:
 *       userName:
 *         type: string
 *       email:
 *         type: string
 *       name:
 *         type: string
 *       password:
 *         type: string
 *   user:
 *     properties:
 *       userName:
 *         type: string
 *       password:
 *         type: string
 */
const User = new Schema({
    email: { type: String, lowercase: true, trim: true, unique: true },
    name: String,
    password: { type: String, unique: true },
    userName: { type: String, unique: true },
});
User.statics = {
    displayAllUsers: function displayAllUsers() {
        return this.find();
    },
    improveUser(id, user) {
        return this.findOneAndUpdate({ _id: id }, { $set: user }, { upsert: true, new: true });
    },
    removeUser(id) {
        return this.findOneAndRemove({ _id: id });
    },
    displayUser(id) {
        return this.findOne({ _id: id });
    },
    addUser(user) {
        const data = new this(user);
        return data.save();
    },
    checkData(userDetail) {
        return this.find({ userName: userDetail });
    },
};
exports.default = mongoose.model('User', User);
//# sourceMappingURL=user.js.map