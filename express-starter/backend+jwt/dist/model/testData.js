"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class TestData {
    constructor() {
        this.data = {
            email: 'test@gmail.com',
            name: 'test',
            password: 'test',
            userName: 'test',
        };
        this.loginData = {
            password: 'test',
            userName: 'test',
        };
        this.loginData1 = {
            password: 'wrongPassword',
            userName: 'wrongUserName',
        };
        this.updateData = {
            email: 'testcase@gmail.com',
            name: 'testcase',
            password: 'testcase',
            userName: 'testcase',
        };
        this.updateData1 = {
            email: 'test',
            name: 'test',
            password: 'test',
        };
    }
}
exports.default = new TestData();
//# sourceMappingURL=testData.js.map