"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const config_1 = require("./config/config");
class Data {
    connect() {
        return mongoose.connect(config_1.config.dbURL, { useNewUrlParser: true });
    }
    testConnect() {
        return mongoose.connect(config_1.config.testDb, { useNewUrlParser: true });
    }
    testClose() {
        return mongoose.disconnect();
    }
}
exports.default = new Data();
//# sourceMappingURL=dbconnection.js.map