"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const bodyParser = require("body-parser");
const express = require("express");
const swaggerUi = require("swagger-ui-express");
const routes_1 = require("./routes/routes");
const swagger_1 = require("./swagger/swagger");
const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use('/swagger-api', swaggerUi.serve, swaggerUi.setup(swagger_1.default));
app.use(routes_1.default);
app.use((err, req, res, next) => {
    console.log('Middleware is working');
    res.send({ status: 'error', message: err.message });
});
exports.default = app;
//# sourceMappingURL=app.js.map