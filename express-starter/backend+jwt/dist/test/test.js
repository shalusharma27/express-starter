"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const request = require("supertest");
const app_1 = require("./../app");
const dbconnection_1 = require("./../dbconnection");
const testData_1 = require("./../model/testData");
const user_1 = require("./../model/user");
describe('Testing', () => {
    let token;
    let id;
    const Id = 1265712;
    beforeAll((done) => {
        return dbconnection_1.default.testConnect().then(() => __awaiter(this, void 0, void 0, function* () {
            console.log('open');
            yield user_1.default.remove({});
            done();
        }));
    });
    describe('POST USER', () => {
        it('Test....post user', (done) => {
            request(app_1.default)
                .post('/users')
                .send(testData_1.default.data)
                .set('Accept', 'application/json')
                .expect(200, done);
        });
        it('Test....wrong format', (done) => {
            request(app_1.default)
                .post('/users')
                .send(testData_1.default.updateData1)
                .set('Accept', 'application/json')
                .expect(500, done);
        });
    });
    describe('USER LOGIN', () => {
        it('Test....login', (done) => {
            request(app_1.default)
                .post('/login')
                .send(testData_1.default.loginData)
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect((res) => {
                token = res.body.token;
            })
                .expect(200, done);
        });
        it('Test....invalid user', (done) => {
            request(app_1.default)
                .post('/login')
                .send(testData_1.default.loginData1)
                .set('Accept', 'application/json')
                .expect(404, done);
        });
    });
    describe('GET ROUTE', () => {
        it('Test....get all users', (done) => {
            request(app_1.default)
                .get('/users')
                .set('Accept', 'application/json')
                .set({ authorization: token })
                .expect('Content-Type', /json/)
                .expect((res) => {
                id = res.body.data[0]._id;
            })
                .expect(200, done);
        });
        it('Test....unathorization access ', (done) => {
            request(app_1.default)
                .get('/users')
                .set('Accept', 'application/json')
                .expect(401, done);
        });
    });
    describe('GET BY ID ROUTE', () => {
        it('Test....get user by ID', (done) => {
            request(app_1.default)
                .get(`/users/${id}`)
                .set('Accept', 'application/json')
                .set({ authorization: token })
                .expect('Content-Type', /json/)
                .expect(200, done);
        });
        it('Test....unathorization access ', (done) => {
            request(app_1.default)
                .get(`/users/${id}`)
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(401, done);
        });
        it('Test....user doesn\'t exist', (done) => {
            request(app_1.default)
                .get(`/users/${Id}`)
                .set({ authorization: token })
                .set('Accept', 'application/json')
                .expect(404, done);
        });
    });
    describe('PUT ROUTE', () => {
        it('Test....put user by ID', (done) => {
            request(app_1.default)
                .put(`/users/${id}`)
                .set('Accept', 'application/json')
                .set({ authorization: token })
                .send(testData_1.default.updateData)
                .expect('Content-Type', /json/)
                .expect(200, done);
        });
        it('Test....unathorization access', (done) => {
            request(app_1.default)
                .put(`/users/${id}`)
                .set('Accept', 'application')
                .send(testData_1.default.updateData)
                .expect(401, done);
        });
        it('Test....user doesn\'t exist', (done) => {
            request(app_1.default)
                .put(`/users/${Id}`)
                .set('Accept', 'application/json')
                .set({ authorization: token })
                .send(testData_1.default.updateData)
                .expect(404, done);
        });
        it('Test....wrong format', (done) => {
            request(app_1.default)
                .put(`/users/${id}`)
                .set('Accept', 'application/json')
                .set({ authorization: token })
                .send(testData_1.default.updateData1)
                .expect(500, done);
        });
    });
    describe('DELETE ROUTE', () => {
        it('Test....delete user by ID', (done) => {
            request(app_1.default)
                .del(`/users/${id}`)
                .set({ authorization: token })
                .expect(200, done);
        });
        it('Test...unauthorization access', (done) => {
            request(app_1.default)
                .del(`/users/${id}`)
                .expect(401, done);
        });
        it('Test....user doesn\'t exist', (done) => {
            request(app_1.default)
                .del(`/users/${Id}`)
                .set({ authorization: token })
                .expect(404, done);
        });
    });
    afterAll((done) => {
        return dbconnection_1.default.testClose().then(() => {
            console.log('closed');
            done();
        });
    });
});
//# sourceMappingURL=test.js.map