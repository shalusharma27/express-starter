"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.config = {
    KEY: 'secret',
    dbURL: 'mongodb://localhost:27017/mydatabase',
    port: 3000,
    testDb: 'mongodb://localhost:27017/testdb',
};
//# sourceMappingURL=config.js.map