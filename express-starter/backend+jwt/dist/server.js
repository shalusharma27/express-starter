"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const app_1 = require("./app");
const config_1 = require("./config/config");
const dbconnection_1 = require("./dbconnection");
dbconnection_1.default.connect().then(() => {
    app_1.default.listen(config_1.config.port, () => {
        console.log('Server is started at ....!!!', config_1.config.port);
    });
});
//# sourceMappingURL=server.js.map