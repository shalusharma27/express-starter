"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const jwt = require("jsonwebtoken");
const config_1 = require("./../config/config");
exports.default = (req, res, next) => {
    try {
        const token = req.headers.authorization;
        const decoded = jwt.verify(token, config_1.config.KEY);
        next();
    }
    catch (error) {
        return res.status(401).json({
            message: 'Unauthorized Access',
        });
    }
};
//# sourceMappingURL=check-auth.js.map