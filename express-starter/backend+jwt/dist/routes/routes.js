"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const check_1 = require("express-validator/check");
const router = express.Router();
const user_1 = require("./../controller/user");
const check_auth_1 = require("./../middleware/check-auth");
const user_2 = require("./../validation/user");
/**
 * @swagger
 * /users:
 *   get:
 *     security:
 *       - Bearer: []
 *     tags:
 *       - users
 *     summary: 'Return All Users'
 *     operationId: 'data.getUser'
 *     description: ''
 *     produces:
 *       - 'application/json'
 *     responses:
 *       200:
 *         description: 'Operation Successful'
 *       401:
 *         description: 'Unauthorized Access'
 *         schema:
 *           $ref: '#/definitions/users'
 */
router.get('/users', check_auth_1.default, user_1.default.getUser);
/**
 * @swagger
 * /users/{id}:
 *   get:
 *     tags:
 *       - users
 *     summary: 'Find User By ID'
 *     description: 'Returns a single user'
 *     operationId: 'data.getUserById'
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         in: path
 *         description: 'Provide ID of User'
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: 'Successful Operation'
 *       404:
 *         description: 'ID Not Found'
 *       401:
 *         description: 'Unauthorized Access'
 *         schema:
 *           $ref: '#/definitions/users'
 */
router.get('/users/:id', check_auth_1.default, user_1.default.getUserById);
/**
 * @swagger
 * /users:
 *   post:
 *     tags:
 *       - users
 *     summary: 'Create User'
 *     description: ''
 *     operationId: 'data.createUser'
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: body
 *         name: body
 *         description: 'Create User Object'
 *         required: true
 *         schema:
 *           $ref: '#/definitions/users'
 *     responses:
 *       200:
 *         description: 'Successfully Operation'
 *       500:
 *         description: 'Internal Server Error'
 */
router.post('/users', user_2.default, (req, res, next) => {
    const notValid = check_1.validationResult(req);
    if (notValid.isEmpty()) {
        next();
    }
    else {
        res.status(500).send(notValid.array());
    }
}, user_1.default.createUser);
/**
 * @swagger
 * /users/{id}:
 *   put:
 *     tags:
 *       - users
 *     summary: 'Edit User'
 *     operationId: 'data.updateUser'
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: 'id'
 *         in: 'path'
 *         description: 'Provide ID of User'
 *         required: true
 *         type: 'string'
 *       - in: 'body'
 *         name: 'body'
 *         description: 'Edit User Object'
 *         required: true
 *         schema:
 *           $ref: '#/definitions/users'
 *     responses:
 *       200:
 *         description: 'User Successfully Edited'
 *       401:
 *         description: 'Unauthorized Access'
 *       404:
 *         description: 'ID Not Found'
 *       500:
 *         description: 'Internal Server Error'
 */
router.put('/users/:id', check_auth_1.default, user_2.default, (req, res, next) => {
    const notValid = check_1.validationResult(req);
    if (notValid.isEmpty()) {
        next();
    }
    else {
        res.status(500).send(notValid.array());
    }
}, user_1.default.updateUser);
/**
 * @swagger
 * /users/{id}:
 *   delete:
 *     tags:
 *       - users
 *     summary: 'Deletes a User'
 *     description: ''
 *     operationId: 'data.deleteUser'
 *     produces:
 *       - 'application/json'
 *     parameters:
 *       - name: id
 *         in: path
 *         description: 'Provide ID of User'
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: 'Operation Successful'
 *       404:
 *         description: 'ID Not Found'
 *       401:
 *         description: 'Unauthorized Access'
 */
router.delete('/users/:id', check_auth_1.default, user_1.default.deleteUser);
/**
 * @swagger
 * /login:
 *   post:
 *     tags:
 *       - login
 *     summary: 'User Login'
 *     description: ''
 *     operationId: 'data.getUser'
 *     produces:
 *       - 'application/json'
 *     parameters:
 *       - in: 'body'
 *         name: 'body'
 *         description: 'Enter User Login Details'
 *         required: true
 *         schema:
 *           $ref: '#/definitions/user'
 *     responses:
 *       200:
 *         description: 'Successfully Logged IN'
 *       404:
 *         description: 'Invalid User'
 */
router.post('/login', user_1.default.createToken);
exports.default = router;
//# sourceMappingURL=routes.js.map