"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const config_1 = require("./../config/config");
const user_1 = require("./../model/user");
class UserController {
    createUser(req, res, next) {
        const userObject = req.body;
        const bcryptPassword = userObject.password;
        bcrypt.hash(bcryptPassword, 10, (err, hash) => {
            userObject.password = hash;
            user_1.default.addUser(userObject)
                .then((doc) => {
                res.send({ status: 'OK', data: doc._id });
            })
                .catch((err1) => res.status(500).send(err1.message));
        });
    }
    updateUser(req, res, next) {
        const id = req.params.id;
        const userObject = req.body;
        const bcryptPass = userObject.password;
        bcrypt.hash(bcryptPass, 10, (err, hash) => {
            userObject.password = hash;
            user_1.default.improveUser(id, userObject)
                .then((doc) => {
                res.send({ status: 'OK', data: doc._id });
            })
                .catch((err1) => {
                if (err1.code === undefined) {
                    res.status(404).json(err1.message);
                }
                else if (err1.code === 11000) {
                    res.status(500).json(err1.message);
                }
                else {
                    res.send(err1.message);
                }
            });
        });
    }
    deleteUser(req, res, next) {
        const id = req.params.id;
        user_1.default.removeUser(id)
            .then((doc) => res.send({ status: 'OK', data: 'null' }))
            .catch((err) => res.status(404).json(err.message));
    }
    getUser(req, res, next) {
        user_1.default.displayAllUsers()
            .then((doc) => res.json({ status: 'OK', data: doc }));
    }
    getUserById(req, res, next) {
        const id = req.params.id;
        user_1.default.displayUser(id)
            .then((doc) => res.json(doc))
            .catch((err) => res.status(404).json(err.message));
    }
    createToken(req, res, next) {
        const userName = req.body.userName;
        const value = req.body.password;
        user_1.default.checkData(userName)
            .then((doc) => {
            if (doc.length > 0) {
                bcrypt.compare(value, doc[0].password, (err, result) => {
                    if (!result) {
                        res.status(404).json('Login Failed');
                    }
                    else {
                        const token = jwt.sign({ _id: doc._id }, config_1.config.KEY);
                        res.send({ status: 'Auth successful', data: doc, token });
                    }
                });
            }
            else {
                throw res.status(404).json('Login Failed');
            }
        })
            .catch((err) => next(new Error(err.message)));
    }
}
exports.default = new UserController();
//# sourceMappingURL=user.js.map