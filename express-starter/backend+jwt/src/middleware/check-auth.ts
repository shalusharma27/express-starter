import { NextFunction, Request, Response } from 'express';
import * as jwt from 'jsonwebtoken';
import { config } from './../config/config';

export default (req: Request, res: Response, next: NextFunction) => {
        try {
            const token: string = req.headers.authorization;
            const decoded: string = jwt.verify(token, config.KEY);
            next();
        }
        catch (error) {
            return res.status(401).json({
                message: 'Unauthorized Access',
            });
        }
    };
