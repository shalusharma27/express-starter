interface IConfig {
    KEY: string;
    dbURL: string;
    port: number;
    testDb: string;
}
export const config: IConfig = {
    KEY: 'secret',
    dbURL: 'mongodb://localhost:27017/mydatabase',
    port: 3000,
    testDb: 'mongodb://localhost:27017/testdb',
};
