import * as mongoose from 'mongoose';
const Schema = mongoose.Schema;
/**
 * @swagger
 * definitions:
 *   users:
 *     properties:
 *       userName:
 *         type: string
 *       email:
 *         type: string
 *       name:
 *         type: string
 *       password:
 *         type: string
 *   user:
 *     properties:
 *       userName:
 *         type: string
 *       password:
 *         type: string
 */

const User = new Schema({
    email: { type: String, lowercase: true, trim: true, unique: true },
    name: String,
    password: { type: String, unique: true },
    userName: { type: String, unique: true },
});

User.statics = {
    displayAllUsers: function displayAllUsers() {
        return this.find();
    },
    improveUser(id: string, user: string) {
        return this.findOneAndUpdate({ _id: id }, { $set: user }, { upsert: true, new: true });
    },
    removeUser(id: string) {
        return this.findOneAndRemove({ _id: id });
    },
    displayUser(id: string) {
        return this.findOne({ _id: id });
    },
    addUser(user: string) {
        const data = new this(user);
        return data.save();
    },
    checkData(userDetail: string) {
        return this.find({ userName: userDetail });
    },
};

interface IUserDocument extends mongoose.Document {
    name: string;
    userName: { type: string, unique: boolean };
    password: { type: string, unique: true };
    email: { type: string, lowercase: boolean, trim: boolean, unique: boolean };
}
interface IUserModel extends mongoose.Model<IUserDocument> {
    displayAllUsers: () => any;
    improveUser(id: string, user: string): any;
    removeUser(id: string): any;
    displayUser(id: string): any;
    addUser(user: string): any;
    checkData(user: string): any;
}

export default mongoose.model<IUserDocument, IUserModel>('User', User);
