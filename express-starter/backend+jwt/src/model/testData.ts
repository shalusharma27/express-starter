interface ITestData {
    data: {
        email: string;
        name: string;
        password: string;
        userName: string;
    };
    loginData: {
        password: string;
        userName: string;
    };
    loginData1: {
        password: string;
        userName: string;
    };
    updateData: {
        email: string;
        name: string;
        password: string;
        userName: string;
    };
    updateData1: any;
}

class TestData implements ITestData {
 public data = {
     email: 'test@gmail.com',
     name: 'test',
     password: 'test',
     userName: 'test',
 };
 public loginData = {
     password: 'test',
     userName: 'test',
 };
 public loginData1 = {
     password: 'wrongPassword',
     userName: 'wrongUserName',
 };
 public updateData = {
     email: 'testcase@gmail.com',
     name: 'testcase',
     password: 'testcase',
     userName: 'testcase',
 };
 public updateData1 = {
     email: 'test',
     name: 'test',
     password: 'test',
 };
}
export default new TestData();
