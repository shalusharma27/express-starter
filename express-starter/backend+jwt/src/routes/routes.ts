import * as express from 'express';
import { NextFunction, Request, Response } from 'express';
import { validationResult } from 'express-validator/check';
const router: express.Router = express.Router();
import data from './../controller/user';
import checkAuth from './../middleware/check-auth';
import userValidator from './../validation/user';

/**
 * @swagger
 * /users:
 *   get:
 *     security:
 *       - Bearer: []
 *     tags:
 *       - users
 *     summary: 'Return All Users'
 *     operationId: 'data.getUser'
 *     description: ''
 *     produces:
 *       - 'application/json'
 *     responses:
 *       200:
 *         description: 'Operation Successful'
 *       401:
 *         description: 'Unauthorized Access'
 *         schema:
 *           $ref: '#/definitions/users'
 */

router.get('/users', checkAuth, data.getUser);

/**
 * @swagger
 * /users/{id}:
 *   get:
 *     tags:
 *       - users
 *     summary: 'Find User By ID'
 *     description: 'Returns a single user'
 *     operationId: 'data.getUserById'
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         in: path
 *         description: 'Provide ID of User'
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: 'Successful Operation'
 *       404:
 *         description: 'ID Not Found'
 *       401:
 *         description: 'Unauthorized Access'
 *         schema:
 *           $ref: '#/definitions/users'
 */

router.get('/users/:id', checkAuth, data.getUserById);

/**
 * @swagger
 * /users:
 *   post:
 *     tags:
 *       - users
 *     summary: 'Create User'
 *     description: ''
 *     operationId: 'data.createUser'
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: body
 *         name: body
 *         description: 'Create User Object'
 *         required: true
 *         schema:
 *           $ref: '#/definitions/users'
 *     responses:
 *       200:
 *         description: 'Successfully Operation'
 *       500:
 *         description: 'Internal Server Error'
 */

router.post('/users', userValidator, (req: Request, res: Response, next: NextFunction) => {
    const notValid = validationResult(req);
    if (notValid.isEmpty()) {
        next();
    }
    else {
        res.status(500).send(notValid.array());
    }
}, data.createUser);

/**
 * @swagger
 * /users/{id}:
 *   put:
 *     tags:
 *       - users
 *     summary: 'Edit User'
 *     operationId: 'data.updateUser'
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: 'id'
 *         in: 'path'
 *         description: 'Provide ID of User'
 *         required: true
 *         type: 'string'
 *       - in: 'body'
 *         name: 'body'
 *         description: 'Edit User Object'
 *         required: true
 *         schema:
 *           $ref: '#/definitions/users'
 *     responses:
 *       200:
 *         description: 'User Successfully Edited'
 *       401:
 *         description: 'Unauthorized Access'
 *       404:
 *         description: 'ID Not Found'
 *       500:
 *         description: 'Internal Server Error'
 */

router.put('/users/:id', checkAuth, userValidator, (req: Request, res: Response, next: NextFunction) => {
    const notValid = validationResult(req);
    if (notValid.isEmpty()) {
        next();
    }
    else {
        res.status(500).send(notValid.array());
    }
}, data.updateUser);

/**
 * @swagger
 * /users/{id}:
 *   delete:
 *     tags:
 *       - users
 *     summary: 'Deletes a User'
 *     description: ''
 *     operationId: 'data.deleteUser'
 *     produces:
 *       - 'application/json'
 *     parameters:
 *       - name: id
 *         in: path
 *         description: 'Provide ID of User'
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: 'Operation Successful'
 *       404:
 *         description: 'ID Not Found'
 *       401:
 *         description: 'Unauthorized Access'
 */

router.delete('/users/:id', checkAuth, data.deleteUser);

/**
 * @swagger
 * /login:
 *   post:
 *     tags:
 *       - login
 *     summary: 'User Login'
 *     description: ''
 *     operationId: 'data.getUser'
 *     produces:
 *       - 'application/json'
 *     parameters:
 *       - in: 'body'
 *         name: 'body'
 *         description: 'Enter User Login Details'
 *         required: true
 *         schema:
 *           $ref: '#/definitions/user'
 *     responses:
 *       200:
 *         description: 'Successfully Logged IN'
 *       404:
 *         description: 'Invalid User'
 */
router.post('/login', data.createToken);

export default router;
