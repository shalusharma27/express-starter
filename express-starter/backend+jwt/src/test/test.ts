import * as request from 'supertest';
import app from './../app';
import Data from './../dbconnection';
import testData from './../model/testData';
import User from './../model/user';

describe('Testing', () => {
    let token: string;
    let id: string;
    const Id: number = 1265712;
    beforeAll((done) => {
        return Data.testConnect().then(async () => {
            console.log('open');
            await User.remove({});
            done();
        });
    });
    describe('POST USER', () => {
        it('Test....post user', (done) => {
            request(app)
                .post('/users')
                .send(testData.data)
                .set('Accept', 'application/json')
                .expect(200, done);
        });
        it('Test....wrong format', (done) => {
            request(app)
                .post('/users')
                .send(testData.updateData1)
                .set('Accept', 'application/json')
                .expect(500, done);
        });
    });
    describe('USER LOGIN', () => {
        it('Test....login', (done) => {
            request(app)
                .post('/login')
                .send(testData.loginData)
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect((res) => {
                    token = res.body.token;
                })
                .expect(200, done);
            });
        it('Test....invalid user', (done) => {
            request(app)
                .post('/login')
                .send(testData.loginData1)
                .set('Accept', 'application/json')
                .expect(404, done);
        });
    });
    describe('GET ROUTE', () => {
        it('Test....get all users', (done) => {
            request(app)
                .get('/users')
                .set('Accept', 'application/json')
                .set({ authorization: token})
                .expect('Content-Type', /json/)
                .expect((res) => {
                    id = res.body.data[0]._id;
                })
                .expect(200, done);
            });
        it('Test....unathorization access ', (done) => {
            request(app)
                .get('/users')
                .set('Accept', 'application/json')
                .expect(401, done);
        });
    });
    describe('GET BY ID ROUTE', () => {
        it('Test....get user by ID', (done) => {
            request(app)
                .get(`/users/${id}`)
                .set('Accept', 'application/json')
                .set({ authorization: token })
                .expect('Content-Type', /json/)
                .expect(200, done);
            });
        it('Test....unathorization access ', (done) => {
            request(app)
                .get(`/users/${id}`)
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(401, done);
        });
        it('Test....user doesn\'t exist', (done) => {
            request(app)
                .get(`/users/${Id}`)
                .set({ authorization: token})
                .set('Accept', 'application/json')
                .expect(404, done);
        });
    });
    describe('PUT ROUTE', () => {
        it('Test....put user by ID', (done) => {
            request(app)
                .put(`/users/${id}`)
                .set('Accept', 'application/json')
                .set({ authorization: token })
                .send(testData.updateData)
                .expect('Content-Type', /json/)
                .expect(200, done);
        });
        it('Test....unathorization access', (done) => {
            request(app)
                .put(`/users/${id}`)
                .set('Accept', 'application')
                .send(testData.updateData)
                .expect(401, done);
        });
        it('Test....user doesn\'t exist', (done) => {
            request(app)
                .put(`/users/${Id}`)
                .set('Accept', 'application/json')
                .set({ authorization: token})
                .send(testData.updateData)
                .expect(404, done);
        });
        it('Test....wrong format', (done) => {
            request(app)
                .put(`/users/${id}`)
                .set('Accept', 'application/json')
                .set({ authorization: token})
                .send(testData.updateData1)
                .expect(500, done);
        });
    });
    describe('DELETE ROUTE', () => {
        it('Test....delete user by ID', (done) => {
            request(app)
                .del(`/users/${id}`)
                .set({ authorization: token })
                .expect(200, done);
        });
        it('Test...unauthorization access', (done) => {
            request(app)
                .del(`/users/${id}`)
                .expect(401, done);
        });
        it('Test....user doesn\'t exist', (done) => {
            request(app)
                .del(`/users/${Id}`)
                .set({authorization: token})
                .expect(404, done);
        });
    });
    afterAll((done) => {
        return Data.testClose().then(() => {
            console.log('closed');
            done();
        });
    });
});
