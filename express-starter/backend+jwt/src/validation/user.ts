import { checkSchema } from 'express-validator/check';

const userValidator = checkSchema({
    email: {
        exists: {
            errorMessage: 'Email cannot be empty',
        },
        in: ['body'],
        isEmail: {
            errorMessage: 'Invalid email',
        },
    },
    name: {
        exists: {
            errorMessage: 'Length of name cannot be less than 1 and greater than 20 characters',
        },
        in: ['body'],
        isAlpha: {
            errorMessage: 'Name must contain only alphabets',
        },
    },
    password: {
        exists: {
            errorMessage: 'password cannot be empty',
        },
        in: ['body'],
        isLength: {
            errorMessage: 'password cannot be empty',
            options: { min: 1 },
        },
    },
    userName: {
        exists: {
            errorMessage: 'userName cannot be empty',
            },
        in: ['body'],
        matches: {
            errorMessage: 'Invalid userName',
            options: '^[a-zA-Z0-9_]+$',
            },
    },
});
export default userValidator;
