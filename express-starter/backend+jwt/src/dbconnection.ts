import * as mongoose from 'mongoose';
import { config } from './config/config';

class Data  {
    public connect() {
        return mongoose.connect(config.dbURL, {useNewUrlParser: true});
    }
    public testConnect() {
        return mongoose.connect(config.testDb, {useNewUrlParser: true});
    }
    public testClose() {
        return mongoose.disconnect();
    }
}

export default new Data();
