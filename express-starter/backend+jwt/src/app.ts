import * as bodyParser from 'body-parser';
import * as express from 'express';
import * as swaggerUi from 'swagger-ui-express';
import router from './routes/routes';
import swaggerSpec from './swagger/swagger';

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use('/swagger-api', swaggerUi.serve, swaggerUi.setup(swaggerSpec));

app.use(router);

app.use((err: Error, req: express.Request, res: express.Response, next: express.NextFunction): void => {
    console.log('Middleware is working');
    res.send({status: 'error' , message: err.message});
});

export default app;
