import app from './app';
import { config } from './config/config';
import Data from './dbconnection';

Data.connect().then(() => {
app.listen(config.port, (): void => {
    console.log('Server is started at ....!!!', config.port);
    });
});
