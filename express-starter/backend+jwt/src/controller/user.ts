import * as bcrypt from 'bcrypt';
import { NextFunction, Request, Response } from 'express';
import * as jwt from 'jsonwebtoken';
import { config } from './../config/config';
import User from './../model/user';

class UserController {
    public createUser(req: Request, res: Response, next: NextFunction): void {
        const userObject: any = req.body;
        const bcryptPassword: string = userObject.password;
        bcrypt.hash(bcryptPassword, 10, (err: Error, hash: string) => {
            userObject.password = hash;
            User.addUser(userObject)
                .then((doc: any) => {
                    res.send({ status: 'OK', data: doc._id });
                })
                .catch((err1: Error) => res.status(500).send(err1.message));
        });
    }
    public updateUser(req: Request, res: Response, next: NextFunction): void {
        const id: string = req.params.id;
        const userObject: any = req.body;
        const bcryptPass = userObject.password;
        bcrypt.hash(bcryptPass, 10, (err: Error, hash: string) => {
            userObject.password = hash;
            User.improveUser(id, userObject)
            .then((doc: any) => {
                res.send({ status: 'OK', data: doc._id });
            })
            .catch((err1: any) => {
                if (err1.code === undefined ) {
                res.status(404).json(err1.message);
                } else if (err1.code === 11000) {
                res.status(500).json(err1.message);
                } else {
                res.send(err1.message);
                }
            });
        });
    }
    public deleteUser(req: Request, res: Response, next: NextFunction): void {
        const id: string = req.params.id;
        User.removeUser(id)
            .then((doc: any) => res.send({ status: 'OK', data: 'null' }))
            .catch((err: Error) => res.status(404).json(err.message));
    }
    public getUser(req: Request, res: Response, next: NextFunction): void {
        User.displayAllUsers()
            .then((doc): any => res.json({ status: 'OK', data: doc }));
    }

    public getUserById(req: Request, res: Response, next: NextFunction): void {
        const id: string = req.params.id;
        User.displayUser(id)
            .then((doc: string) => res.json(doc))
            .catch((err: Error) => res.status(404).json(err.message));
    }

    public createToken(req: Request, res: Response, next: NextFunction) {
        const userName: string = req.body.userName;
        const value: string = req.body.password;
        User.checkData(userName)
            .then((doc: any) => {
                if (doc.length > 0) {
                    bcrypt.compare(value, doc[0].password, (err: Error, result: boolean) => {
                        if (! result) {
                            res.status(404).json('Login Failed');
                        }
                        else {
                            const token = jwt.sign({ _id: doc._id }, config.KEY);
                            res.send({ status: 'Auth successful', data: doc, token });
                        }
                    });
                } else {
                throw res.status(404).json('Login Failed');
                }
            })
            .catch((err: Error) => next(new Error(err.message)));
    }
}
export default new UserController();
