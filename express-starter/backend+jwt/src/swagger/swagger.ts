import swaggerJSDoc = require('swagger-jsdoc');

interface ISwaggerDefinition {
    basePath: string;
    host: string;
    info: {
      description: string;
      parameters: any;
      paths: any;
      responses: any;
      swagger: any;
      title: string;
      version: string;
    };
    security: any;
    securityDefinitions: {
      Bearer: {
        in: string;
        name: string;
        type: string;
      };
    };
}
const swaggerDefinition: ISwaggerDefinition = {
    basePath: '/',
    host: 'localhost:3000',
    info: {
      description: 'Demonstrating how to describe a RESTful API with Swagger',
      parameters: { },
      paths: { },
      responses: { },
      swagger: '2.0',
      title: 'Node Swagger API',
      version: '1.0.0',
    },
    security: [
      { Bearer: [] },
    ],
    securityDefinitions: {
      Bearer: {
        in: 'Headers',
        name: 'Authorization',
        type: 'apiKey',
      },
    },
  };
const options = {
    apis: ['./dist/**/*.js', './dist/router.js'],
    swaggerDefinition,
  };
const swaggerSpec = swaggerJSDoc(options);
export default swaggerSpec;
