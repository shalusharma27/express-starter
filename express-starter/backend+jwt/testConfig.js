module.exports = {
    globals: {
    'ts-test': {
    tsConfigFile: 'tsconfig.json'
    }
    },
    moduleFileExtensions: [
    'ts',
    'js'
    ],
    testMatch: [
    '**/test/*.test.(js)'
    ],
    testEnvironment: 'node'
    };